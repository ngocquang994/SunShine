package com.example.android.sunshine.app;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.android.sunshine.app.data.WeatherContract;
import com.example.android.sunshine.app.databinding.ListItemForecastBinding;
import com.example.android.sunshine.app.databinding.ListItemForecastTodayBinding;
import com.example.android.sunshine.app.model.WeatherDetail;

/**
 * Created by Nguyễn Đăng Quang on 03-Oct-16.
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.MyViewHolder> {
    private static final int VIEW_TODAY = 0;
    private static final int VIEW_FUTURE_DAY = 1;
    private Cursor mCursor;
    private String daynameString, descString, lowString, highString;
    public static int mIcon;
    private Context mContext;
    private View statusContainer;

    public ForecastAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId = -1;
        switch (viewType) {
            case VIEW_TODAY: {
                layoutId = R.layout.list_item_forecast_today;
                break;
            }
            case VIEW_FUTURE_DAY: {
                layoutId = R.layout.list_item_forecast;
                break;
            }
        }
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        statusContainer = inflater.inflate(layoutId, parent, false);
        return new MyViewHolder(statusContainer, viewType);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        mCursor.moveToPosition(position);
        int weatherId = mCursor.getInt(mCursor.getColumnIndex(WeatherContract.WeatherEntry.COL_WEATHER_ID));
        if (position == 0) {
            mIcon = Utility.getArtResourceForWeatherCondition(weatherId);
        } else {
            mIcon = Utility.getIconResourceForWeatherCondition(weatherId);
        }


        // read date from cursor
        long dateMilis = mCursor.getLong(ForecastFragment.COL_WEATHER_DATE);
        daynameString = Utility.getFriendlyDayString(mContext, dateMilis);
        String description = mCursor.getString(ForecastFragment.COL_WEATHER_DESC);
        descString = description;
        double high = mCursor.getDouble(ForecastFragment.COL_WEATHER_MAX_TEMP);
        highString = Utility.formatTemperature(mContext, high);
        double low = mCursor.getDouble(ForecastFragment.COL_WEATHER_MIN_TEMP);
        lowString = Utility.formatTemperature(mContext, low);
        holder.imageView.setImageResource(mIcon);
        WeatherDetail weatherDetail = new WeatherDetail(daynameString, descString, lowString, highString, mIcon);
        holder.bind(weatherDetail);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCursor.moveToPosition(position);
                String locationSetting = Utility.getPreferredLocation(mContext);
                Intent intent = new Intent(mContext, DetailActivity.class)
                        .setData(WeatherContract.WeatherEntry.buildWeatherLocationWithDate(
                                locationSetting, mCursor.getLong(ForecastFragment.COL_WEATHER_DATE)
                        ));
                mContext.startActivity(intent);
            }

        });

    }


    @BindingAdapter({"android:src"})
    public static void setImageViewResource(
            ImageView imageView, int resource) {
        imageView.setImageResource(mIcon);
    }


    @Override
    public int getItemCount() {
        return mCursor == null ? 0 : mCursor.getCount();
    }

    public void swapCursor(Cursor newCursor) {
        mCursor = newCursor;
        notifyDataSetChanged();
    }

    public Cursor getCursor() {
        return mCursor;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0) ? VIEW_TODAY : VIEW_FUTURE_DAY;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private ListItemForecastBinding listItemForecastBinding;
        private ListItemForecastTodayBinding listItemForecasetTodayBinding;
        private int viewType;
        private ImageView imageView;

        public MyViewHolder(View view, int viewType) {
            super(view);
            this.viewType = viewType;
            switch (viewType) {
                case VIEW_TODAY: {
                    listItemForecasetTodayBinding = DataBindingUtil.bind(itemView);
                    break;
                }
                case VIEW_FUTURE_DAY: {
                    listItemForecastBinding = DataBindingUtil.bind(itemView);
                    break;
                }
            }
            imageView = (ImageView) view.findViewById(R.id.list_item_icon);
        }

        public void bind(WeatherDetail weatherDetail) {
            switch (viewType) {
                case VIEW_TODAY: {
                    listItemForecasetTodayBinding.setWeather(weatherDetail);
                    break;
                }
                case VIEW_FUTURE_DAY: {
                    listItemForecastBinding.setWeather(weatherDetail);
                    break;
                }
            }
        }
    }
}
