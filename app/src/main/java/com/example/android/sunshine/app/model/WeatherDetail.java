package com.example.android.sunshine.app.model;

/**
 * Created by Kai on 25-Sep-16.
 */

public class WeatherDetail {
    private String dayname;
    private String monthname;
    private String wind;
    private String desc;
    private String humi;
    private String press;
    private String lowtemp;
    private String hightemp;
    private int micon;


    public int getIcon() {
        return micon;
    }

    public void setIcon(int micon) {
        this.micon = micon;
    }

    public String getDayname() {
        return dayname;
    }

    public String getMonthname() {
        return monthname;
    }

    public String getWind() {
        return wind;
    }

    public String getDesc() {
        return desc;
    }

    public String getHumi() {
        return humi;
    }

    public String getPress() {
        return press;
    }

    public String getLowtemp() {
        return lowtemp;
    }

    public String getHightemp() {
        return hightemp;
    }

    public void setDayname(String dayname) {
        this.dayname = dayname;
    }

    public void setMonthname(String monthname) {
        this.monthname = monthname;
    }

    public void setWind(String wind) {
        this.wind = wind;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setHumi(String humi) {
        this.humi = humi;
    }

    public void setPress(String press) {
        this.press = press;
    }

    public void setLowtemp(String lowtemp) {
        this.lowtemp = lowtemp;
    }

    public void setHightemp(String hightemp) {
        this.hightemp = hightemp;
    }


    public WeatherDetail(String dayname, String monthname, String wind, String desc, String humi,
                         String press, String lowtemp, String hightemp, int micon) {
        this.dayname = dayname;
        this.monthname = monthname;
        this.wind = wind;
        this.desc = desc;
        this.humi = humi;
        this.press = press;
        this.lowtemp = lowtemp;
        this.hightemp = hightemp;
        this.micon = micon;
    }
    public WeatherDetail(String dayname, String desc, String lowtemp, String hightemp, int micon) {
        this.dayname = dayname;
        this.desc = desc;
        this.lowtemp = lowtemp;
        this.hightemp = hightemp;
        this.micon = micon;


    }

}
