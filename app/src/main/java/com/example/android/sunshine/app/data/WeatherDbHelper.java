
package com.example.android.sunshine.app.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.android.sunshine.app.data.WeatherContract.LocationEntry;
import com.example.android.sunshine.app.data.WeatherContract.WeatherEntry;


public class WeatherDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;

    static final String DATABASE_NAME = "weather.db";

    public WeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_LOCATION_TABLE = "CREATE TABLE " + LocationEntry.TABLE_NAME + " (" +
                LocationEntry._ID + " INTEGER PRIMARY KEY," +
                LocationEntry.COL_LOCATION_SETTING + " TEXT UNIQUE NOT NULL, " +
                LocationEntry.COL_CITY_NAME + " TEXT NOT NULL, " +
                LocationEntry.COL_COORD_LAT + " REAL NOT NULL, " +
                LocationEntry.COL_COORD_LONG + " REAL NOT NULL " +
                " );";

        final String SQL_CREATE_WEATHER_TABLE = "CREATE TABLE " + WeatherEntry.TABLE_NAME + " (" +

                WeatherEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                WeatherEntry.COL_LOC_KEY + " INTEGER NOT NULL, " +
                WeatherEntry.COL_DATE + " INTEGER NOT NULL, " +
                WeatherEntry.COL_SHORT_DESC + " TEXT NOT NULL, " +
                WeatherEntry.COL_WEATHER_ID + " INTEGER NOT NULL," +

                WeatherEntry.COL_MIN_TEMP + " REAL NOT NULL, " +
                WeatherEntry.COL_MAX_TEMP + " REAL NOT NULL, " +

                WeatherEntry.COL_HUMIDITY + " REAL NOT NULL, " +
                WeatherEntry.COL_PRESSURE + " REAL NOT NULL, " +
                WeatherEntry.COL_WIND_SPEED + " REAL NOT NULL, " +
                WeatherEntry.COL_DEGREES + " REAL NOT NULL, " +

                " FOREIGN KEY (" + WeatherEntry.COL_LOC_KEY + ") REFERENCES " +
                LocationEntry.TABLE_NAME + " (" + LocationEntry._ID + "), " +
                " UNIQUE (" + WeatherEntry.COL_DATE + ", " +
                WeatherEntry.COL_LOC_KEY + ") ON CONFLICT REPLACE);";

        sqLiteDatabase.execSQL(SQL_CREATE_LOCATION_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_WEATHER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + LocationEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + WeatherEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
