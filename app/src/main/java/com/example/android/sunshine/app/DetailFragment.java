package com.example.android.sunshine.app;

import android.content.Intent;
import android.database.Cursor;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.android.sunshine.app.data.WeatherContract;
import com.example.android.sunshine.app.databinding.FragmentDetailBinding;
import com.example.android.sunshine.app.model.WeatherDetail;



/**
 * Created by Kai on 20-Sep-16.
 */

public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String LOG_TAG = DetailFragment.class.getSimpleName();
    static final String DETAIL_URI = "URI";
    private Uri detailuri;
    private static final String FORECAST_SHARE_HASHTAG = " - By SunShine !";
    private String mForecastStr;
    private ShareActionProvider mShareActionProvider;
    private static final int DETAIL_LOADER = 0;
    private WeatherDetail weatherDetail;
    private String dateName, dateText, description, highString, lowString, humidityString, wind, pressureString;
    private static int micon;
    private FragmentDetailBinding fragmentDetailBinding;

    public DetailFragment() {
        setHasOptionsMenu(true);
    }

    private static final String[] DETAIL_COLUMNS = {
            WeatherContract.WeatherEntry.TABLE_NAME + "." + WeatherContract.WeatherEntry._ID,
            WeatherContract.WeatherEntry.COL_DATE,
            WeatherContract.WeatherEntry.COL_SHORT_DESC,
            WeatherContract.WeatherEntry.COL_MAX_TEMP,
            WeatherContract.WeatherEntry.COL_MIN_TEMP,
            WeatherContract.WeatherEntry.COL_HUMIDITY,
            WeatherContract.WeatherEntry.COL_PRESSURE,
            WeatherContract.WeatherEntry.COL_WIND_SPEED,
            WeatherContract.WeatherEntry.COL_DEGREES,
            WeatherContract.WeatherEntry.COL_WEATHER_ID,
            WeatherContract.LocationEntry.COL_LOCATION_SETTING
    };

    public static final int COL_WEATHER_ID = 9;
    public static final int COL_WEATHER_DATE = 1;
    public static final int COL_WEATHER_DESC = 2;
    public static final int COL_WEATHER_MAX_TEMP = 3;
    public static final int COL_WEATHER_MIN_TEMP = 4;
    public static final int COL_WEATHER_HUMIDITY = 5;
    public static final int COL_WEATHER_PRESSURE = 6;
    public static final int COL_WEATHER_WIND_SPEED = 7;
    public static final int COL_WEATHER_DEGREES = 8;
    public static final int COL_WEATHER_CONDITION_ID = 10;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle argument = getArguments();
        if (argument != null) {
            detailuri = argument.getParcelable(DetailFragment.DETAIL_URI);
        }
        fragmentDetailBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_detail, container,
                false);
        View view = fragmentDetailBinding.getRoot();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.detailfragment, menu);
        MenuItem menuItem = menu.findItem(R.id.action_share);

        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);
        if (mForecastStr != null) {
            mShareActionProvider.setShareIntent(createShareForecastIntent());
        }
    }


    private Intent createShareForecastIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, mForecastStr + FORECAST_SHARE_HASHTAG);
        return shareIntent;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(DETAIL_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    void onLocationChanged(String newLocation) {

        Uri uri = detailuri;
        if (null != uri) {
            long date = WeatherContract.WeatherEntry.getDateFromUri(uri);
            Uri updatedUri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(newLocation, date);
            detailuri = updatedUri;
            getLoaderManager().restartLoader(DETAIL_LOADER, null, this);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        if (null != detailuri) {
            return new CursorLoader(
                    getActivity(),
                    detailuri,
                    DETAIL_COLUMNS,
                    null,
                    null,
                    null
            );
        }
        return null;
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null && data.moveToFirst()) {

            int weatherId = data.getInt(data.getColumnIndex(WeatherContract.WeatherEntry.COL_WEATHER_ID));

            micon = Utility.getArtResourceForWeatherCondition(weatherId);
            Log.d("Check image", micon + "");
            long date = data.getLong(COL_WEATHER_DATE);
            dateName = Utility.getDayName(getActivity(), date);
            dateText = Utility.getFormattedMonthDay(getActivity(), date);
            description = data.getString(COL_WEATHER_DESC);

            boolean isMetric = Utility.isMetric(getActivity());
            double high = data.getDouble(COL_WEATHER_MAX_TEMP);
            highString = Utility.formatTemperature(getActivity(), high);

            double low = data.getDouble(COL_WEATHER_MIN_TEMP);
            lowString = Utility.formatTemperature(getActivity(), low);

            //Do am
            float humidity = data.getFloat(COL_WEATHER_HUMIDITY);
            humidityString = getActivity().getString(R.string.format_humidity, humidity);
            //gio
            float windSpeed = data.getFloat(COL_WEATHER_WIND_SPEED);
            float winDir = data.getFloat(COL_WEATHER_DEGREES);
            wind = Utility.getFormattedWind(getActivity(), windSpeed, winDir);

            //Suc ep
            float pressure = data.getFloat(COL_WEATHER_PRESSURE);
            pressureString = getActivity().getString(R.string.format_pressure, pressure);
            //    mPressureView.setText(getActivity().getString(R.string.format_pressure, pressure));
            mForecastStr = String.format("%s - %s - %s/%s", dateName + " ", dateText, description, highString, lowString);
            weatherDetail = new WeatherDetail(dateName, dateText, wind, description, humidityString, pressureString, lowString,
                    highString, micon);
            fragmentDetailBinding.setWeatherdetail(weatherDetail);
            Log.d("Check ???", dateName + "  " + dateText + "  " + wind + "  " + description + "  " + humidityString + "  " + pressureString + "  " + lowString + "  " + highString);
            if (mShareActionProvider != null) {
                mShareActionProvider.setShareIntent(createShareForecastIntent());
            }
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    @BindingAdapter({"bind:src"})
    public static void setImageViewResource(ImageView imageView, int resource) {
        imageView.setImageResource(micon);
    }
}

