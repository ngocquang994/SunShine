package com.example.android.sunshine.app;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by Kai on 14-Sep-16.
 */
public class FragmentSetting  extends PreferenceFragment{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.fragment_preference_setting);
    }

}
